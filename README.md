# Firefox settings

## Getting Started

1. Generate SSH keys.

    ```sh
    ssh-keygen
    ```

2. Copy the contents of `id_rsa.pub` to the clipboard (save it to Bitwarden as well).

    ```sh
    pbcopy < "$HOME/.ssh/id_rsa.pub"
    ```

3. Add the public key to [GitLab](https://gitlab.com/profile/keys).

4. Clone this repo to `$HOME/projects/firefox`.

    ```sh
    git clone git@gitlab.com:h0adp0re/firefox.git "$HOME/projects/firefox"
    ```

5. Add `user.name` & `user.email` to git config.

    ```sh
    cd "$HOME/projects/firefox"
    git config --add user.name "h0adp0re"
    git config --add user.email "1249378-h0adp0re@users.noreply.gitlab.com"
    ```

## Firefox

```sh
ln -sf "$HOME/projects/firefox/user.js" "$HOME/Library/Application Support/Firefox/Profiles/<DEFAULT_PROFILE>/user.js"
ln -sf "$HOME/projects/firefox/chrome" "$HOME/Library/Application Support/Firefox/Profiles/<DEFAULT_PROFILE>/chrome"
```

## Firefox Developer Edition

```sh
ln -sf "$HOME/projects/firefox/user_dev.js" "$HOME/Library/Application Support/Firefox/Profiles/<DEV_PROFILE>/user.js"
ln -sf "$HOME/projects/firefox/chrome" "$HOME/Library/Application Support/Firefox/Profiles/<DEV_PROFILE>/chrome"
```

## Search Engines

```sh
ln -sf "$HOME/projects/firefox/search.json.mozlz4" "$HOME/Library/Application Support/Firefox/Profiles/<DEV_PROFILE>/search.json.mozlz4"
ln -sf "$HOME/projects/firefox/search.json.mozlz4" "$HOME/Library/Application Support/Firefox/Profiles/<DEFAULT_PROFILE>/search.json.mozlz4"
```

## Firefox Settings

1. Log in to Firefox.
2. Import extension settings from the next section.

## Extension Settings

- Cookie AutoDelete
- CanvasBlocker
- Dark Reader
- Enhancer for YouTube
- Stylus
- Tree Style Tab
- uBlock Origin

### Safari

- AdGuard
- Vimari

### Chrome

- Vimium

## Extension Shortcuts

### Bitwarden

- Open vault popup `shift ⌘ Y`
- Auto-fill `shift ⌘ L`
- Generate and copy a new random password to the clipboard `shift ⌘ 9`

### Dark Reader

- Toggle extension `shift ⎇  D`
- Toggle current site `shift ⎇  A`

### Firefox Multi-Account Containers

- Open containers panel `^.`

### Stylus

- Manage `shift ⎇  E`
- Turn all styles off `shift ⎇  S`

### Tree Style Tab

- Toggle "Tree Style Tab" Sidebar `F2`
- Scroll Tabs Up by Lines `shift ⎇  Up`
- Scroll Tabs Down by Lines `shift ⎇  Down`
- Move Current Tab Up `shift ⎇  K`
- Move Current Tree Up `shift ⎇  H`
- Move Current Tab Down `shift ⎇  J`
- Move Current Tree Down `shift ⎇  L`

### Tridactyl

- Run `:installnative`
- Paste the command into a terminal emulator

### uBlock Origin

- Enter element picker mode `⎇  ⌘ X`
- Relax blocking mode `shift ⎇  Q`

## Extensions

- [Amazon Container](https://addons.mozilla.org/en-US/firefox/addon/contain-amazon/)
  - Isolates your Amazon activity from the rest of your web activity in order to prevent Amazon from tracking you outside of any Amazon websites via third party cookies.
- [Bitwarden - Free Password Manager](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/)
  - An open-source and (mostly) free password manager.
- [CanvasBlocker](https://addons.mozilla.org/en-US/firefox/addon/canvasblocker/)
  - Prevents websites from using some JavaScript APIs to fingerprint you.
- [Codext](https://addons.mozilla.org/en-US/firefox/addon/codext/)
  - Code viewer and editor embedded in your browser!
- [Cookie AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)
  - Auto-delete unused cookies from your closed tabs while keeping the ones you want.
- [CSS Exfil Protection](https://addons.mozilla.org/en-US/firefox/addon/css-exfil-protection/)
  - This plugin sanitizes and blocks any CSS rules which may be designed to steal data.
- [Dark Reader](https://addons.mozilla.org/en-US/firefox/addon/darkreader/)
  - Dark mode for every website.
- [Decentraleyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/)
  - Protects you against tracking through "free", centralized, content delivery.
- [Enhancer for YouTube](https://addons.mozilla.org/en-US/firefox/addon/enhancer-for-youtube/)
  - A multitool for a better YouTube experience.
- [Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/)
  - Isolates your Facebook activity from the rest of your web activity.
- [Firefox Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)
  - Multi-Account Containers helps you keep all the parts of your online life contained in different tabs.
- [Google Container](https://addons.mozilla.org/en-US/firefox/addon/google-container/)
  - Isolates your Google activity from the rest of your web activity in order to prevent Google from tracking you outside of any Google websites via third party cookies.
- [Smart Prevent Duplicate Tabs](https://addons.mozilla.org/en-US/firefox/addon/smart-prevent-duplicate-tabs/)
  - Prevent duplicate tabs.
- [Search by Image](https://addons.mozilla.org/en-US/firefox/addon/search_by_image/)
  - A powerful reverse image search tool, with support for various search engines.
- [Sidebery](https://addons.mozilla.org/en-US/firefox/addon/sidebery/)
  - Vertical tabs tree and bookmarks in the sidebar.
- [Smart Referer](https://addons.mozilla.org/en-US/firefox/addon/smart-referer/)
  - Limit referer information leak.
- [SponsorBlock - Skip Sponsorships on YouTube](https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/)
  - Easily skip YouTube video sponsors.
- [Stylus](https://addons.mozilla.org/en-US/firefox/addon/styl-us/)
  - Custom user styles manager.
- [Tab Counter Plus](https://addons.mozilla.org/en-US/firefox/addon/tab-counter-plus/)
  - Shows the number of tabs in each window.
- [Tridactyl](https://addons.mozilla.org/en-US/firefox/addon/tridactyl-vim/)
  - Replace Firefox's control mechanism with one modelled on Vim.
- [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
  - An efficient blocker: easy on memory and CPU footprint, and yet can load and enforce thousands more filters than other popular blockers out there.

## Theme

- [Matte Black (White)](https://addons.mozilla.org/en-US/firefox/addon/matte-black-theme/)
